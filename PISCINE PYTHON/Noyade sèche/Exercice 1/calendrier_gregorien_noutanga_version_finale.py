# ARIANE NOUTANG L3 M2I #


# FONCTIONS #
def introduction():
    print("\n******************************************************************************")
    print("* Bonjour et bienvenue dans notre Calendrier Grégorien 'Nouvelle Génération' *")
    print("******************************************************************************\n")


def signature():
    print("******************* Programmé par Ariane NOUTANG - M2i ***********************\n")


# CODE PRINCIPAL #
''' gestion de l'année '''
introduction()
annee = input("Veuillez saisir l'année souhaitée : ")
nb = int(annee[2:]) + int(annee[2:])//4
# print(nb)


''' gestion du mois '''
mois = int(input("Maintenant, veuillez saisir le mois : "))
while mois < 1 or mois > 12:
    mois = int(input("Veuillez le re-saisir : "))
if mois == 1 or mois == 10:
    nb = nb + 0
elif mois == 2 or mois == 3 or mois == 11:
    nb = nb + 3
elif mois == 4 or mois == 7:
    nb = nb + 6
elif mois == 5:
    nb = nb + 1
elif mois == 6:
    nb = nb + 4
elif mois == 8:
    nb = nb + 2
elif mois == 9 or mois == 12:
    nb = nb + 5
# print(nb)


''' gestion du jour '''
jour = int(input("Et enfin, le jour : "))
while jour < 1 or jour > 31:
    jour = int(input("Veuillez le re-saisir : "))
    if jour >= 1 & jour <= 31:
        break
nb = nb + jour
# print(nb)


''' gestion de l'annee bissextile '''
if (mois == 1) or (mois == 2):
    nb = nb - 1


''' gestion du siècle '''
siecle = int(annee[:2])
if siecle == 16:
    nb = nb + 6
elif siecle == 17:
    nb = nb + 4
elif siecle == 18:
    nb = nb + 2
elif siecle == 19:
    nb = nb + 0
elif siecle == 20:
    nb = nb + 6
elif siecle == 21:
    nb = nb + 4


''' calcul complémentaire '''
nb = nb % 7
# print(nb)


''' affichage du jour '''
if nb == 0:
    print("\n* Ce jour était donc le DIMANCHE %02.0d/%02.0d/%s *\n\n" % (jour, mois, annee))
elif nb == 1:
    print("\n* Ce jour était donc le LUNDI %02.0d/%02.0d/%s *\n\n" % (jour, mois, annee))
elif nb == 2:
    print("\n* Ce jour était donc le MARDI %02.0d/%02.0d/%s *\n\n" % (jour, mois, annee))
elif nb == 3:
    print("\n* Ce jour était donc le MERCREDI %02.0d/%02.0d/%s *\n\n" % (jour, mois, annee))
elif nb == 4:
    print("\n* Ce jour était donc le JEUDI %02.0d/%02.0d/%s *\n\n" % (jour, mois, annee))
elif nb == 5:
    print("\n* Ce jour était donc le VENDREDI %02.0d/%02.0d/%s *\n\n" % (jour, mois, annee))
elif nb == 6:
    print("\n* Ce jour était donc le SAMEDI %02.0d/%02.0d/%s *\n\n" % (jour, mois, annee))


signature()


# ARIANE NOUTANG L3 M2I #